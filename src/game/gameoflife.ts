import { Cell } from "./cell";

export class GameOfLife {
    
    private state: Cell[][] = [];
    constructor(size = 15) {
        for(let x = 0; x < size; x++) {
            for(let y = 0; y  < size; y++) {
                if(!this.state[x]) {
                    this.state[x] = [];
                }
                this.state[x][y] = new Cell();
            }
        }
    }

    grid() {
        return this.state;
    }

    cellState(x: number, y: number): boolean {
        return this.state[x][y].isAlive();
    }
    inject(x: number, y:number) {
        this.state[x][y] = new Cell(true);
    }

    turn() {
        const cellMap = new Map<Cell,number>();
        for(let x = 0; x < this.state.length; x++) {
            for(let y = 0; y < this.state[x].length; y++) {
                const currentCell = this.state[x][y];
                cellMap.set(currentCell, this.getCellNeighboors(x,y));
                
            }

        }
        cellMap.forEach((neighboors, cell) => cell.nextGeneration(neighboors));
    }

    totalCellAlive() {
        let count = 0;
        for(let row of this.state) {
            for(let cell of row) {
                if(cell.isAlive()) {
                    count++;
                }
            }
        }
        return count;
    }

    getCellNeighboors(i:number, j:number):number {
        let rowLimit = this.state.length-1;
        let columnLimit = this.state[0].length-1;
        let aliveCount = 0;
        for(let x = Math.max(0, i-1); x <= Math.min(i+1, rowLimit); x++) {
            for(let y = Math.max(0, j-1); y <= Math.min(j+1, columnLimit); y++) {
                if((x !== i || y !== j) && this.state[x][y].isAlive()) {
                    aliveCount++;
                }
            }
        }
        return aliveCount;
    }
}