

export class Cell {

    nextGeneration(neighboors: number) {
        if(this.alive) {
            if(neighboors > 3 || neighboors <= 1) {
                this.alive = false;
            }
        } else {
            if(neighboors === 3) {
                this.alive = true;
            }
        }
    }
    

    constructor(private alive = false){}

    isAlive():boolean {
        return this.alive;
    }

}