

export class Connect4 {

    private state: String[][];
    private turn: boolean = false;

    constructor() {
        this.state = [
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
        ]
    }

    grid() {
        return this.state;
    }

    play(column: number) {
        const player = this.turn ? 'red' : 'yellow';
        let played = false;
        for (let i = this.state.length - 1; i >= 0; i--) {
            if (this.state[i][column] === null) {
                this.state[i][column] = player;
                played = true;
                break;
            }
        }
        if (!played) {
            throw new Error("Can't play here. Column is full.");
        }
        this.turn = !this.turn;
    }
}