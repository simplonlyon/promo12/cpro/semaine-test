import * as readline from 'readline';

export class Guesser {
    private toGuess:number;
    
    constructor(answer?:number) {
        if(answer) {
            this.toGuess = answer;
        } else {
            this.toGuess = Math.floor(Math.random() * 11);

        }
    }


    display(toDisplay:string) {
        console.log(toDisplay);
    }

    isAnwser(guess:number):boolean {
        return guess === this.toGuess;
    }

    inputToGuess(input:string):number {
        
        const guess = Number(input);
        if(isNaN(guess)) {
            throw new Error('Input is not a number');
        }
        return guess;
    }
    

    guess() {

        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question('What number am I thinking of ?', input => {
            try {
                const guess = this.inputToGuess(input);
                if(this.isAnwser(guess)) {
                    this.display('yes');
                } else {
                    this.guess();
                }
            } catch (error) {
                this.display('Input should be a number...');
                this.guess();
            }
        });

    }

}