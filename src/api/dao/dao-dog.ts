import { ResultSetHeader, RowDataPacket } from "mysql2";
import { Dog } from "../entities/dog";
import { connection } from "./connection";


export class DaoDog {

    constructor(private db = connection){}

    async getAll():Promise<Dog[]> {
        const [results] = await this.db.query<RowDataPacket[]>('SELECT * FROM dog');
        return results.map(row => new Dog(row['name'], row['breed'], row['age'], row['id']));
    }
    async add(dog: Dog) {
        const [result] = await this.db.query<ResultSetHeader>('INSERT INTO dog (name,breed,age) VALUES (?,?,?)', [
            dog.name,
            dog.breed,
            dog.age
        ]);

        return result.insertId;
        
    }
}