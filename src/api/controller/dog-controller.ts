import { Request, Response } from "express";
import { DaoDog } from "../dao/dao-dog";



export class DogController {
    

    constructor(private daoDog = new DaoDog()) {}

    async getAllDogs(req:Request, resp:Response){

        const dogs = await this.daoDog.getAll();

        resp.json(dogs);
    }
}