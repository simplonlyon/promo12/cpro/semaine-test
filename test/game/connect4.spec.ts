import { expect } from "chai";
import { Connect4 } from "../../src/game/connect4";

describe('Connect 4', () => {
    let connect4:Connect4;

    beforeEach(() => {
        connect4 = new Connect4();
    })

    it('should be able to play a move', () => {

        connect4.play(0);
    });


    it('should display the grid state', () => {

        const grid = connect4.grid();

        expect(grid).to.be.deep.equal([
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
        ]);
    });

    it('should put token at bottom of column when played', () => {
        connect4.play(0);

        const grid = connect4.grid();

        expect(grid).to.be.deep.equal([
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            ['yellow', null, null, null, null],
        ]);
    });

    it('should alternate between yellow and red when played', () => {
        connect4.play(0);
        connect4.play(1);

        const grid = connect4.grid();

        expect(grid).to.be.deep.equal([
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, null, null, null, null],
            ['yellow', 'red', null, null, null],
        ]);
    });
    it('should stack token if played on same column', () => {
        connect4.play(0);
        connect4.play(0);
        connect4.play(0);

        const grid = connect4.grid();

        expect(grid).to.be.deep.equal([
            [null, null, null, null, null],
            [null, null, null, null, null],
            ['yellow', null, null, null, null],
            ['red', null, null, null, null],
            ['yellow', null, null, null, null],
        ]);
    });

    it('should print error when played on full column', () => {
        connect4.play(0);
        connect4.play(0);
        connect4.play(0);
        connect4.play(0);
        connect4.play(0);
        expect(() => connect4.play(0)).to.throw("Can't play here. Column is full.");
        


    });
});