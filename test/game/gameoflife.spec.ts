import { expect } from "chai";
import { Cell } from "../../src/game/cell";
import { GameOfLife } from "../../src/game/gameoflife";


describe('Game Of Life', () => {

    it('should instantiate with a grid full of cells', () => {
        const gameOfLife = new GameOfLife();

        expect(gameOfLife.grid()).to.be.deep.equal(
            new Array(15).fill(new Array(15).fill(new Cell()))
        );
    });

    it('should return specific cell state', () => {
        const gameOfLife = new GameOfLife();

        expect(gameOfLife.cellState(0,0)).to.be.false;
    });

    it('should be able to injecta living cell in grid', () => {
        const gameOfLife = new GameOfLife();

        gameOfLife.inject(0, 0);

        expect(gameOfLife.cellState(0,0)).to.be.true;

    });
    it('should give total alive cells', () => {
        const gameOfLife = new GameOfLife();

        gameOfLife.inject(1,1);
        gameOfLife.inject(1,3);
        gameOfLife.inject(2,2);
        

        expect(gameOfLife.totalCellAlive()).to.be.equal(3);
    })

    it('should give alive cell around a given cell', () => {
        const gameOfLife = new GameOfLife();

        gameOfLife.inject(1,1);
        gameOfLife.inject(1,3);
        gameOfLife.inject(2,2);

        expect(gameOfLife.getCellNeighboors(1, 2)).to.be.equal(3);
    })
    it('should give alive cell around a corner cell', () => {
        const gameOfLife = new GameOfLife();

        gameOfLife.inject(1,1);
        gameOfLife.inject(0,1);
        gameOfLife.inject(1,0);

        expect(gameOfLife.getCellNeighboors(0, 0)).to.be.equal(3);
    })

    it('should change generation', () => {
        const gameOfLife = new GameOfLife();

        gameOfLife.inject(0,0);

        gameOfLife.turn();

        expect(gameOfLife.cellState(0,0)).to.be.false;
    })


});