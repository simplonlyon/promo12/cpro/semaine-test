import { expect } from "chai";
import { Cell } from "../../src/game/cell";

describe('Game Of Life Cell', () =>{

    it('should instantiate with a not alive state', () => {
        const cell  = new Cell();

        expect(cell.isAlive()).to.be.false;

    });

    it('should die on next generation if 3+ neighboors', () => {
        const cell = new Cell(true);

        cell.nextGeneration(4);

        expect(cell.isAlive()).to.be.false;


    });

    it('should die on next generation if 1 on less neighboors', () => {
        const cell = new Cell(true);

        cell.nextGeneration(1);

        expect(cell.isAlive()).to.be.false;


    });
    it('should live on next generation if 2 or 3 neighboors', () => {
        const cell = new Cell(true);

        cell.nextGeneration(2);

        expect(cell.isAlive()).to.be.true;


    });
    it('should resurect on next generation if 3 neighboors', () => {
        const cell = new Cell();

        cell.nextGeneration(3);

        expect(cell.isAlive()).to.be.true;


    });
    it('should stay dead on next generation if less than 3 neighboors', () => {
        const cell = new Cell();

        cell.nextGeneration(2);

        expect(cell.isAlive()).to.be.false;


    });
    it('should stay dead on next generation if more than 3 neighboors', () => {
        const cell = new Cell();

        cell.nextGeneration(4);

        expect(cell.isAlive()).to.be.false;


    });
});