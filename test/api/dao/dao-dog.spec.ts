import { expect } from "chai";
import { connection } from "../../../src/api/dao/connection";
import { DaoDog } from "../../../src/api/dao/dao-dog"
import { Dog } from "../../../src/api/entities/dog";

describe('DaoDog', () => {
    

    beforeEach(async ()=> {
        await connection.query('START TRANSACTION');
    });
    afterEach(async() => {
        await connection.query('ROLLBACK');

    });

    it('should fetch all dogs from database', async () => {
        const dao = new DaoDog();
        const dogs = await dao.getAll();

        expect(dogs.length).to.be.equal(3);
        expect(dogs[0]).to.be.deep.equal({
            name: 'name1',
            breed: 'breed1',
            age: 1,
            id: 1
        });
    });

    it('should add a new dog in database', async () => {
        const dao = new DaoDog();
        const id = await dao.add(new Dog('test', 'test', 1));

        expect(id).to.be.a('number');
    })
})
