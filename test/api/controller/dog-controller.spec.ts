import { DogController } from "../../../src/api/controller/dog-controller";
import { expect, spy, use } from "chai";
import * as chaiSpy from 'chai-spies';
import { DaoDog } from "../../../src/api/dao/dao-dog";
import { Dog } from "../../../src/api/entities/dog";
import { Request, Response } from "express";
use(chaiSpy);

describe('Dog Controller', () => {


    describe('getAll method', () => {

        it('should return dogs in json format', async () => {

            const daoSpy = new DaoDog();

            spy.on(daoSpy, 'getAll', async () => [
                new Dog('test1', 'test1', 1, 1),
                new Dog('test2', 'test2', 2, 2),
                new Dog('test3', 'test3', 3, 3)
            ]);

            const response = {
                json: spy()
            };


            const controller = new DogController(daoSpy);

            await controller.getAllDogs({} as Request, <unknown>response as Response);

            expect(response.json).to.have.been.called.with([
                new Dog('test1', 'test1', 1, 1),
                new Dog('test2', 'test2', 2, 2),
                new Dog('test3', 'test3', 3, 3)
            ]);
        });
    });
});