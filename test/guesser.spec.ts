import { expect, spy, use } from "chai";
import * as chaiSpy from 'chai-spies';
import { Guesser } from "../src/guesser";
import * as readline from 'readline';

use(chaiSpy);

describe('Guesser', () => {
    let guesser:Guesser;

    beforeEach(() => {
        guesser = new Guesser(4);
    });

    describe('isAnswer', () => {

        it('should return true when right guess', () => {
         
    
            const actual = guesser.isAnwser(4);
    
            expect(actual).to.be.true;
    
        });
    
        it('should return false when wrong guess', () => {
            
    
            const actual = guesser.isAnwser(6);
    
            expect(actual).to.be.false;
    
        });
    })

    it('should display "hello world" in console', () => {

        const logSpy = spy(console.log);

        console.log = logSpy;



        guesser.display('hello world');

        expect(logSpy).to.have.been.called.with('hello world');
    });


    it('isAnswer should return true when right guess with spy', () => {
        spy.on(Math, 'random', () => 0.4);

        const guesser = new Guesser();

        const actual = guesser.isAnwser(4);

        expect(actual).to.be.true;

    });


})